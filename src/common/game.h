#ifndef GAME_H
#define GAME_H

#include "dealerservice.h"
#include "player.h"
#include <filesystem>
#include <QVector>
#include <stdexcept>
#include <QMutex>

class Game : public QObject
{
    Q_OBJECT
public:
    explicit Game(QObject *parent = nullptr);
    void start(const QVector<QString> &usernames);

public slots:
    void executeMSG(const QJsonObject &obj);

private:
    void addPlayer(const QString &username);
    void playCardFromDeck();
    void playCard(unsigned playerIndex, unsigned cardIndex);
    void drawCard(unsigned playerIndex, unsigned numOfCards);
    void reverseOrientation();
    void skipPlayer();
    void changeColor(const QString &chosenColor);
    void nextPlayer();
    void unoCheck(const int playerIndex);
    void cardPostEffects(const Card &card, const int index);
    void endTurn();
    int indexOfNextPlayer() const;
    void msg_chat(unsigned int playerIndex,const QString &msgstring);

signals:
    void jsonContent(const QJsonObject &jsonObject);
    void gameEnded();

public:
    QVector<Player> Players() const;
private:
    QVector<Player> m_Players;
    unsigned m_CurrentPlayer;
    bool m_ClockwiseOrientation;
    bool m_Uno;
    DealerService m_DealerService;
    bool weHaveWinner = false;
};

#endif // GAME_H
