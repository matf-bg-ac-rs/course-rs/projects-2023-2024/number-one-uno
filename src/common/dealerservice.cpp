#include "dealerservice.h"

DealerService::DealerService()
    : m_Deck(Deck()), m_Pile(Pile())
{
    initializeDeck();
}

auto DealerService::deal(Card& card) -> bool
{
    if(m_Deck.isEmpty()) {
        refill();
    }
    if(m_Deck.isEmpty()) {
        return false;
    }
    card = m_Deck.giveCard();
    return true;
}

void DealerService::takeCard(Card& card)
{
    m_Deck.addCard(card);
}


void DealerService::discard(Card card)
{
    m_Pile.addCard(card);
    m_Pile.setTopCardColor(card.colorName());
}

void DealerService::shuffle()
{
    m_Deck.shuffle();
}

void DealerService::refill()
{
    if(m_Pile.isEmpty())
        return;
    Card card = m_Pile.giveCard();
    while(!m_Pile.isEmpty()) {
        m_Deck.addCard(m_Pile.giveCard());
    }
    m_Deck.shuffle();
    m_Pile.addCard(card);
}

auto DealerService::topCard() -> Card
{
    if(m_Pile.isEmpty())
        return Card();
    return m_Pile.topCard();
}

auto DealerService::getTopCardColor() const -> QString
{
    return m_Pile.getTopCardColor();
}
void DealerService::setTopCardColor(const QString &newTopCardColor)
{
    m_Pile.setTopCardColor(newTopCardColor);
}

void DealerService::initializeDeck()
{
    for (unsigned int i = 0; i < 10; ++i){
        Card card(Card::COLOR::RED, i);
        m_Deck.addCard(card);
        m_Deck.addCard(card);
    }
    for (unsigned int i = 0; i < 10; ++i){
        Card card(Card::COLOR::PINK, i);
        m_Deck.addCard(card);
        m_Deck.addCard(card);
    }
    for (unsigned int i = 0; i < 10; ++i){
        Card card(Card::COLOR::PURPLE, i);
        m_Deck.addCard(card);
        m_Deck.addCard(card);
    }
    for (unsigned int i = 0; i < 10; ++i){
        Card card(Card::COLOR::TURQUOISE, i);
        m_Deck.addCard(card);
        m_Deck.addCard(card);
    }
    for (unsigned int i= 0;i<2;++i){
        Card card_rr(Card::RED,Card::REVERSE);
        Card card_rpink(Card::PINK,Card::REVERSE);
        Card card_rpurple(Card::PURPLE,Card::REVERSE);
        Card card_rt(Card::TURQUOISE,Card::REVERSE);

        Card card_sr(Card::RED,Card::SKIP);
        Card card_spink(Card::PURPLE,Card::SKIP);
        Card card_spurple(Card::PINK,Card::SKIP);
        Card card_st(Card::TURQUOISE,Card::SKIP);

        Card card_dtr(Card::RED,Card::DRAW_TWO);
        Card card_dtpurple(Card::PURPLE,Card::DRAW_TWO);
        Card card_dtpink(Card::PINK,Card::DRAW_TWO);
        Card card_dtt(Card::TURQUOISE,Card::DRAW_TWO);

        m_Deck.addCard(card_rr);
        m_Deck.addCard(card_rpink);
        m_Deck.addCard(card_rpurple);
        m_Deck.addCard(card_rt);

        m_Deck.addCard(card_sr);
        m_Deck.addCard(card_spink);
        m_Deck.addCard(card_spurple);
        m_Deck.addCard(card_st);

        m_Deck.addCard(card_dtr);
        m_Deck.addCard(card_dtpurple);
        m_Deck.addCard(card_dtpink);
        m_Deck.addCard(card_dtt);
    }

    for (unsigned int i=0; i<4; ++i){
        Card card_wf(Card::WILD_DRAW_FOUR);
        Card card_w(Card::WILD);

        m_Deck.addCard(card_w);
        m_Deck.addCard(card_wf);
    }
    m_Deck.shuffle();
}
