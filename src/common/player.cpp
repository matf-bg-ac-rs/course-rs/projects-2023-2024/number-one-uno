#include "player.h"

Player::Player(const QString &username)
    : m_username(username)
{}

auto Player::username() const -> QString
{
    return m_username;
}

auto Player::hand() const -> QVector<Card>
{
    return m_hand;
}

auto Player::hasOnlyOneCard() const -> bool
{
    return numberOfCards() == 1;
}

auto Player::hasNoCards() const -> bool
{
    return numberOfCards() == 0;
}

auto Player::numberOfCards() const -> qsizetype
{
    return m_hand.size();
}

void Player::pushCardToHand(const Card &card)
{
    m_hand.push_back(card);
}

auto Player::peekCard(unsigned int index) const -> Card
{
    return m_hand.at(index);
}

auto Player::peekCard() const -> Card
{
    return m_hand.at(m_hand.size() - 1);
}

auto Player::throwCard(unsigned int index) -> Card
{
    Card card = Card(m_hand.at(index));
    m_hand.erase(m_hand.begin() + index);
    return card;
}

auto Player::UnoChecked() const -> bool
{
    return m_UnoChecked;
}

void Player::setUnoChecked(bool newUnoChecked)
{
    m_UnoChecked = newUnoChecked;
}


