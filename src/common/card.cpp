#include "card.h"

Card::Card() = default;

Card::~Card() = default;

Card::Card(Card::COLOR color, int value)
    : m_color(color), m_cardType(CARD_TYPE::NUMERIC), m_value(value) {}

Card::Card(Card::COLOR color, Card::CARD_TYPE cardType)
    : m_color(color), m_cardType(cardType) {}

Card::Card(Card::CARD_TYPE cardType) : m_cardType(cardType) {}

Card::Card(const Card &other)
    : m_color(other.color()), m_cardType(other.cardType()),
      m_value(other.value()) {}

auto Card::color() const -> Card::COLOR { return m_color; }

auto Card::cardType() const -> Card::CARD_TYPE { return m_cardType; }

auto Card::value() const -> int { return m_value; }

auto Card::operator==(const Card &other) const -> bool {
        return color() == other.color() && cardType() == other.cardType() &&
               value() == other.value();
}

auto Card::operator!=(const Card &other) const -> bool { return !(*this == other); }

auto Card::operator=(const Card &other) -> Card & {
        m_color = other.color();
        m_cardType = other.cardType();
        m_value = other.value();
        return *this;
}

auto Card::isCompatibleWith(const Card &other,
                            const QString &topCardColor) const -> bool {
        if (color() == COLOR::BLACK)
                return true;
        if (color() == colorStrToEnum(topCardColor))
                return true;
        if (other.cardType() == Card::CARD_TYPE::NUMERIC)
                return value() == other.value();
        return other.cardType() == cardType();
}

auto Card::toString() const -> std::string {
        return "[ " + colorName().toStdString() + " - " +
               cardTypeName().toStdString() + " - " + std::to_string(m_value) +
               " ]";
}

auto operator<<(std::ostream &ostr, const Card &card) -> std::ostream & {
        return ostr << card.toString();
}

auto Card::colorName() const -> QString {
        switch (m_color) {
        case COLOR::RED:
                return "RED";
        case COLOR::PINK:
                return "PINK";
        case COLOR::TURQUOISE:
                return "TURQUOISE";
        case COLOR::PURPLE:
                return "PURPLE";
        case COLOR::BLACK:
                return "BLACK";
        default:
                qDebug() << "colorName";
                throw QString{"UNKNOWN_COLOR(colorName)"};
        }
}

auto Card::cardTypeName() const -> QString {
        switch (m_cardType) {
        case CARD_TYPE::NUMERIC:
                return "NUMERIC";
        case CARD_TYPE::SKIP:
                return "SKIP";
        case CARD_TYPE::REVERSE:
                return "REVERSE";
        case CARD_TYPE::DRAW_TWO:
                return "DRAW_TWO";
        case CARD_TYPE::WILD:
                return "WILD";
        case CARD_TYPE::WILD_DRAW_FOUR:
                return "WILD_DRAW_FOUR";
        default:
                throw QString{"UNKNOWN_CARD_TYPE"};
        }
}

auto Card::colorStrToEnum(const QString &str) -> Card::COLOR {
        if (str == "RED")
                return COLOR::RED;
        if (str == "PINK")
                return COLOR::PINK;
        if (str == "TURQUOISE")
                return COLOR::TURQUOISE;
        if (str == "PURPLE")
                return COLOR::PURPLE;
        if (str == "BLACK")
                return COLOR::BLACK;

        // return COLOR::RED;
        throw std::invalid_argument("UNKNOWN_COLOR");
        // throw QString{"UNKNOWN_COLOR"};
}

auto Card::cardTypeStrToEnum(const QString &str) -> Card::CARD_TYPE {
        if (str == "NUMERIC")
                return CARD_TYPE::NUMERIC;
        if (str == "SKIP")
                return CARD_TYPE::SKIP;
        if (str == "REVERSE")
                return CARD_TYPE::REVERSE;
        if (str == "DRAW_TWO")
                return CARD_TYPE::DRAW_TWO;
        if (str == "WILD")
                return CARD_TYPE::WILD;
        if (str == "WILD_DRAW_FOUR")
                return CARD_TYPE::WILD_DRAW_FOUR;

        throw std::invalid_argument("UNKNOWN_CARD_TYPE");
}

auto Card::toVariant() const -> QVariant {
        QVariantMap map;
        map.insert("color", colorName());
        map.insert("cardType", cardTypeName());
        map.insert("value", m_value);
        return map;
}

void Card::fromVariant(const QVariant &variant) {
        QVariantMap map = variant.toMap();
        m_color = colorStrToEnum(map["color"].toString());
        m_cardType = cardTypeStrToEnum(map["cardType"].toString());
        m_value = map["value"].toInt();
}
