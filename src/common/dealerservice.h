#ifndef DEALERSERVICE_H
#define DEALERSERVICE_H

#include "deck.h"
#include "pile.h"
#include "card.h"
#include <QString>

class DealerService
{
public:
    DealerService();
    bool deal(Card& card);
    Card topCard();
    void takeCard(Card& card);
    void discard(Card card);
    void refill();
    void shuffle();

    QString getTopCardColor() const;
    void setTopCardColor(const QString &newTopCardColor);

private:
    void initializeDeck();

private:
    Deck m_Deck;
    Pile m_Pile;

};

#endif // DEALERSERVICE_H
