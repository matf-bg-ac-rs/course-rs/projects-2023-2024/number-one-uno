#ifndef MAINSERVER_H
#define MAINSERVER_H

#include <QTcpServer>

class GameWorker;
class QThread;
class QJsonObject;

class MainServer : public QTcpServer
{
    Q_OBJECT
    Q_DISABLE_COPY(MainServer)

public:
    explicit MainServer(QObject *parent = nullptr);
    ~MainServer();

private:
    const int m_idealThreadCount;
    QVector<QThread *> m_availableThreads;

    QMap<qintptr, QTcpSocket*> socketMap;
    QMap<QString, GameWorker*> workerMap;
    QMap<QTcpSocket*, QString> gameIDBySocket;

    void incomingConnection(qintptr socketDescriptor) override;

    void createGame(QString username, QTcpSocket* adminSocket);
    void joinGame(QString username, QTcpSocket* playerSocket, QString gameID);

    bool gameExists(QString gameID);

    QString generateUniqueGameId();
    QString generateRandom4LetterCombination();

signals:
    void jsonReceived(QString gameID, const QJsonObject &json);

private slots:
    void onDisconnected();
    void receiveJson();
    void parseJson(QTcpSocket* sender, const QJsonObject &json);
    void sendJson(qintptr socketDesc, const QJsonObject &json);
    void destroyGame(QString gameID);

};

#endif // MAINSERVER_H
