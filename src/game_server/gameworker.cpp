#include "gameworker.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QObject>

GameWorker::GameWorker(QString gameID, QObject *parent)
    : gameID(gameID)
    , QObject(parent)
{
    qDebug() << "Created game" << gameID;
}

auto GameWorker::getGameID() const -> QString
{
    return gameID;
}

auto GameWorker::usernameExists(QString username) -> bool
{
    return usernames.contains(username);
}

auto GameWorker::numOfPlayers() -> int
{
    return usernames.size();
}

auto GameWorker::isAdmin(qintptr adminSocketDesc) -> bool
{
    if (socketDescToPlayerIdxMap.contains(adminSocketDesc)){
        return socketDescToPlayerIdxMap[adminSocketDesc] == 0;
    }
    return false;
}

void GameWorker::addPlayerToLobby(QString username, qintptr socket)
{
    qDebug() << "Player" << username << "joined game" << getGameID();

    usernames.append(username);
    socketDescToPlayerIdxMap[socket] = usernames.indexOf(username);

    QJsonObject json = {
        {"type", "lobbyState"},
        {"action", "playerJoined"},
        {"gameID", getGameID()},
        {"players", QJsonArray::fromStringList(usernames)},
    };
    broadcast(json);
}

void GameWorker::removePlayerFromLobby(qintptr socket)
{

    int playerIdx = socketDescToPlayerIdxMap[socket];
    qDebug() << getGameID() << ": Player" << usernames[playerIdx] << "removed.";

    socketDescToPlayerIdxMap.remove(socket);
    usernames.removeAt(playerIdx);

    QJsonObject json = {
        {"type", "lobbyState"},
        {"gameID", getGameID()},
        {"players", QJsonArray::fromStringList(usernames)},
    };

    broadcast(json);

}

void GameWorker::startGame()
{
    qDebug() << "Game" << getGameID() << "starting";

    m_game = new Game();
    connect(m_game, &Game::jsonContent, this, &GameWorker::broadcast);
    connect(m_game, &Game::gameEnded, this, &GameWorker::endGame);
    connect(this, &GameWorker::playerActionSignal, m_game, &Game::executeMSG);

    QJsonObject json = {
        {"type", "startGame"},
        {"players", QJsonArray::fromStringList(usernames)}
    };
    broadcast(json);

    m_game->start(usernames);
}

auto GameWorker::isInProgress() -> bool
{
    return m_game != nullptr;
}

void GameWorker::playerLeftWhileGameInProgress(qintptr socketDesc){

    int playerIdx = socketDescToPlayerIdxMap[socketDesc];
    socketDescToPlayerIdxMap.remove(socketDesc);

    const QJsonObject json = {
        {"type", "error"},
        {"text", QString("%1 left :(").arg(usernames[playerIdx])},
    };
    broadcast(json);

    endGame();
}

void GameWorker::endGame()
{
    m_game->deleteLater();
    m_game = nullptr;
    emit gameEnded(getGameID());
}

void GameWorker::receiveJson(const QJsonObject &json)
{
    emit playerActionSignal(json);
}

void GameWorker::broadcast(const QJsonObject &json)
{
    qDebug() << getGameID() << "broadcasting: " << QJsonDocument(json).toJson(QJsonDocument::Indented);

    auto sockets = socketDescToPlayerIdxMap.keys();
    for (auto socket = sockets.constBegin(); socket != sockets.constEnd(); ++socket) {
        emit sendJsonSignal(*socket, json);
    }
}
