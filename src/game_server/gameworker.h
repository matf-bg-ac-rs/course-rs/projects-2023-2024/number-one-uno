#ifndef GAMEWORKER_H
#define GAMEWORKER_H

#include "game.h"

#include <QObject>
#include <QTcpSocket>

class GameWorker : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(GameWorker)
public:

    explicit GameWorker(QString gameID, QObject *parent = nullptr);
    QString getGameID() const;

    void addPlayerToLobby(QString username, qintptr socket);
    void removePlayerFromLobby(qintptr socket);
    void playerLeftWhileGameInProgress(qintptr socketDesc);

    int numOfPlayers();
    bool isAdmin(qintptr adminSocketDesc);
    bool usernameExists(QString username);
    bool isInProgress();

    void broadcast(const QJsonObject &json);

public slots:
    void startGame();
    void receiveJson(const QJsonObject &json);
    void endGame();

signals:
    void sendJsonSignal(qintptr player, const QJsonObject &json);
    void playerActionSignal(const QJsonObject &json);
    void gameEnded(QString gameID);

private:
    QString gameID;

    Game* m_game = nullptr;
    QVector<QString> usernames;
    QMap<qintptr, uint> socketDescToPlayerIdxMap;
};

#endif // GAMEWORKER_H
