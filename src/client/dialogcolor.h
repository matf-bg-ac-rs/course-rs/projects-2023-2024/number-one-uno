#ifndef DIALOGCOLOR_H
#define DIALOGCOLOR_H

#include <QObject>
#include <QDialog>
#include <QCloseEvent>

class DialogColor : public QDialog {
    Q_OBJECT

public:
    DialogColor(QWidget* parent = nullptr);

protected:
    void closeEvent(QCloseEvent *event) override;

signals:
    void colorSelected(const QString& color);

};

#endif // DIALOGCOLOR_H
