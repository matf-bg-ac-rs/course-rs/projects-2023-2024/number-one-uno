#include <QPainter>
#include <QPixmap>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsObject>
#include <iostream>
#include <sstream>
#include "carditem.h"
#include "servercommunicationmanager.h"
#include "card.h"

CardItem::CardItem(Card *card)
    : QGraphicsObject()
    , m_card(card)
{}

CardItem::~CardItem()
{
    delete m_card;
}

Card* CardItem::card() const
{
    return m_card;
}

bool CardItem::visible() const
{
    return m_visible;
}

int CardItem::Width() const
{
    if (!m_visible)
    {
        return m_width * m_scalingFactor;
    }
    else
    {
        return m_width;
    }
}

int CardItem::Height() const
{
    if(!m_visible)
    {
        return m_height * m_scalingFactor;
    }
    else
    {
        return m_height;
    }
}

QRectF CardItem::boundingRect() const
{
    return QRectF(0, 0, Width(), Height());
}

void CardItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    QPixmap image;
    QString path;

    if(!m_visible)
    {
        path = QString("../../resources/images/cards/back.jpeg");
    }
    else
    {
        path = LoadImage(this->card());
    }

    image.load(path);
    painter->drawPixmap(0, 0, Width(), Height(),image);
}

void CardItem::setVisible(bool visible)
{
    m_visible = visible;
}

QString CardItem::LoadImage(Card* card)
{
    std::string path;
    std::string imgPath = "../../resources/images/cards/";
    std::string color = card->colorName().toLower().toStdString();
    path = imgPath + color;

    if(card->cardType() == Card::CARD_TYPE::NUMERIC)
    {
        path = path + std::to_string(card->value()) + ".png";
    }
    else if(card->color() == Card::COLOR::BLACK)
    {
        if (card->cardType() == Card::CARD_TYPE::WILD_DRAW_FOUR)
        {
            path = path + "0" + ".png";
        }
        else
        {
            path = path + "1" + ".png";
        }
    }
    else if(card->cardType() == Card::CARD_TYPE::SKIP)
    {
        path = path + "10" + ".png";
    }
    else if(card->cardType() == Card::CARD_TYPE::REVERSE)
    {
        path = path + "11" + ".png";
    }
    else if(card->cardType() == Card::CARD_TYPE::DRAW_TWO)
    {
        path = path + "12" + ".png";

    }

    return QString::fromStdString(path);
}
