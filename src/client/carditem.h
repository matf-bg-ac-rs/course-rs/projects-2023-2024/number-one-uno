#ifndef CARDITEM_H
#define CARDITEM_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsObject>
#include "card.h"

class Card;

class CardItem : public QGraphicsObject
{
    Q_OBJECT

public:

    CardItem(Card *card);
    ~CardItem();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void setVisible(bool visible);

    Card* card() const;
    bool visible() const;
    int Width() const;
    int Height() const;

    static QString LoadImage(Card* card);

signals:
    void CardPressed(CardItem *item);

private:
    Card *m_card;

    int m_width = 70;
    int m_height = 110;

    float m_scalingFactor = 0.7; // Za ostale igrace
    bool m_visible = true;
};

#endif // CARDITEM_H
