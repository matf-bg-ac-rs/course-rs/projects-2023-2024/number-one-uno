#include "mainwindow.hpp"
#include "servercommunicationmanager.h"
#include "game.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (!ServerCommunicationManager::instance().connectToServer("127.0.0.1", 1967)) {
        qDebug() << "Can't connect to server";
        return 1;
    }

    MainWindow w;
    w.show();
    return a.exec();
}
