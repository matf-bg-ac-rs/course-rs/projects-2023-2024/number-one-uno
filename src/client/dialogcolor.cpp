#include "dialogcolor.h"
#include "servercommunicationmanager.h"
#include "gamewindow.h"
#include <QLabel>
#include <QComboBox>
#include <QPushButton>

DialogColor::DialogColor(QWidget* parent) : QDialog(parent)
{
    setWindowModality(Qt::WindowModality::ApplicationModal);
    setWindowTitle("Dialog Window");
    setMinimumWidth(300);
    setMinimumHeight(200);

    QLabel* lbl = new QLabel("Choose color:", this);
    lbl->setGeometry(100, 75, 120, 20);

    QComboBox* cbChooseColor = new QComboBox(this);
    cbChooseColor->addItems({"PINK", "RED", "PURPLE", "TURQUOISE"});
    cbChooseColor->setObjectName("cbChooseColor");
    cbChooseColor->setGeometry(200, 75, 120, 20);

    QPushButton* pbSubmit = new QPushButton("Submit", this);
    pbSubmit->setGeometry(250, 200, 80, 20);
    connect(pbSubmit, &QPushButton::clicked, this, [=]()
    {
        GameWindow * gw_parent = dynamic_cast<GameWindow *>(parent);
        gw_parent->setSelectedColor(cbChooseColor->currentText());
        ServerCommunicationManager::instance().onDialogWildCardClicked(gw_parent->selectedColor());
        accept();
    });
}

void DialogColor::closeEvent(QCloseEvent *event)
{
    event->ignore();
}


