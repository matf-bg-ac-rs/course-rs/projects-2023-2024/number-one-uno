#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QObject>
#include <QDebug>
#include <QDialog>
#include <iostream>
#include <string>
#include <QStandardItemModel>
#include "carditem.h"
#include <QMediaPlayer>
#include <QAudioOutput>
#include <QUrl>

class QGraphicsScene;
class Card;
class CardItem;

namespace Ui {
class GameWindow;
}

class GameWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit GameWindow(QVector<QString> m_playerNamesList, int m_mainPlayerIndex, QWidget *parent = 0, QString imgPath = "");
    ~GameWindow();
    void OpenDialogChooseColor(); // For WILD cards
    void ChangePileCard(CardItem *item); // Change pile image
    void SetUpAppearance(QString imgPath);
    void ConnectSceneAndView(QVector <QString> playerNames);

    Card *CreateCard(QString cardType, QString color, int value);

    int mainPlayerIndex() const;
    void setSelectedColor(const QString &newSelectedColor);
    QString selectedColor() const;

signals:

    void newCardAdded(CardItem *card, int index);
    void RemoveCard(CardItem *item);



public slots:

    void addNewCard(int index, QString cardType, QString color, int value);
    void CardPressed(CardItem *item);

private slots:

    void initializePile(QString cardType, QString color, int value);
    void cardPlayed(int playerIndex, int cardIndex);
    void setColor(QString color);
    void initTurn(int currPlayerIdx);

    void on_pushButton_clicked();
    void on_sendButton_clicked();
    void on_volumeSlider_valueChanged(int value);
    void on_pb_clicked();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::GameWindow *ui;
    QVector <QGraphicsScene *> m_players;

    int m_mainPlayerIndex;
    QString m_selectedColor;

    QStandardItemModel * m_chatModel;
    QVector<QString> m_playerNamesList;
    int activePlayer = 0;

    QString m_imgPath;
    void unoCalled(int playerIndex);
    void msgCalled(int playerIndex,const QString &text);

    QMediaPlayer *mediaPlayer;
    QAudioOutput *audioOutput;
};

#endif // GAMEWINDOW_H
