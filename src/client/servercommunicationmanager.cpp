#include "servercommunicationmanager.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

ServerCommunicationManager::ServerCommunicationManager()
    : socket(new QTcpSocket(this))
{
    connect(socket, &QAbstractSocket::errorOccurred, this, &ServerCommunicationManager::handleError);
    connect(socket, &QTcpSocket::readyRead, this, &ServerCommunicationManager::receiveJson);
}

bool ServerCommunicationManager::connectToServer(const QString& serverAddress, int port)
{

    socket->connectToHost(serverAddress, port);

    if (!socket->waitForConnected(5000)) {
        return false;
    }

    qDebug() << "Connected to the server!";
    return true;
}

bool ServerCommunicationManager::isConnected()
{
    return socket->state() == QAbstractSocket::ConnectedState;
}

void ServerCommunicationManager::handleError(QAbstractSocket::SocketError socketError)
{
    qDebug() << "Socket error:" << socket->errorString();
    socket->close();
    socket->deleteLater();
    emit showError(socket->errorString());
}

void ServerCommunicationManager::sendRequest(const QJsonObject &json)
{
    const QByteArray jsonData = QJsonDocument(json).toJson();
    QDataStream socketStream(socket);
    socketStream << jsonData;
}

void ServerCommunicationManager::createGame(QString username)
{
    QJsonObject jsonObject;
    jsonObject["type"] = "createGame";
    jsonObject["username"] = username;
    sendRequest(jsonObject);
}

void ServerCommunicationManager::joinGame(QString username, QString gameID)
{
    QJsonObject jsonObject;
    jsonObject["type"] = "joinGame";
    jsonObject["username"] = username;
    jsonObject["gameID"] = gameID;
    sendRequest(jsonObject);
}

void ServerCommunicationManager::startGame()
{
    QJsonObject jsonObject;
    jsonObject["type"] = "startGame";
    sendRequest(jsonObject);
}

void ServerCommunicationManager::receiveJson()
{
    QByteArray jsonData;

    QDataStream socketStream(socket);
    for (;;) {
        socketStream.startTransaction();
        socketStream >> jsonData;
        if (socketStream.commitTransaction()) {
            QJsonParseError parseError;
            const QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &parseError);
            if (parseError.error == QJsonParseError::NoError) {
                if (jsonDoc.isObject())
                    parseJson(jsonDoc.object());
                else
                    qDebug() << QLatin1String("Invalid message: ") + QString::fromUtf8(jsonData);
            } else {
                qDebug() <<  QLatin1String("Invalid message: ") + QString::fromUtf8(jsonData);
            }
        } else {
            break;
        }
    }
}

void ServerCommunicationManager::parseJson(const QJsonObject &json)
{
    const QJsonValue typeVal = json.value(QLatin1String("type"));
    if (typeVal.isNull() || !typeVal.isString())
        return;
    const QString type = typeVal.toString();

    if (type == "error")
    {
        QString msg;
        if (json.contains("text"))
        {
            msg = json["text"].toString();
        }
        emit showError(msg);
    }

    if (type == "goToStartScreen")
    {
        QString msg;
        if (json.contains("text"))
        {
            msg = json["text"].toString();
        }
        emit goToStartScreenSignal();
        emit showError(msg);
    }

    if (type == "lobbyState")
    {
        QString gameID;
        QVector<QString> players;
        if (json.contains("gameID"))
        {
            gameID = json["gameID"].toString();
        }
        if (json.contains("players"))
        {
            QJsonArray playersArray = json["players"].toArray();
            for (const QJsonValue& playerValue : playersArray)
            {
                if (playerValue.isString())
                {
                    players.append(playerValue.toString());
                }
            }
        }

        if (players.length() == 1)
        {
            emit goToLobby(gameID, players);
        }else
        {
            emit joinLobby(gameID, players);
        }
    }

    if (type == "startGame")
    {
        QVector<QString> players;

        if (!json.contains("players")) return;

        QJsonArray playersArray = json["players"].toArray();
        for (const QJsonValue& playerValue : playersArray)
        {
            if (!playerValue.isString())
                continue;
            players.append(playerValue.toString());
        }

        emit startGameSignal(players);
    }

    if (type == "firstCardPlayed")
    {
        QJsonObject cardObject = json.value("card").toObject();
        QString cardType = cardObject.value("cardType").toString();
        QString color = cardObject.value("color").toString();
        int value = cardObject.value("value").toInt();

        emit initializePileSignal(cardType, color, value);
    }

    if (type == "cardDealt")
    {
        if (!json.contains("playerIndex") || !json.contains("card")) return;

        int playerIndex = json["playerIndex"].toInt();
        QJsonObject cardObject = json.value("card").toObject();
        QString cardType = cardObject.value("cardType").toString();
        QString color = cardObject.value("color").toString();
        int value = cardObject.value("value").toInt();

        emit cardDealtSignal(playerIndex, cardType, color, value);
    }

    if (type == "cardPlayed")
    {
        int playerIndex = -1;
        if (json.contains("playerIndex"))
        {
            playerIndex = json["playerIndex"].toInt();
        }
        int cardIndex = -1;
        if (json.contains("cardIndex"))
        {
            cardIndex = json["cardIndex"].toInt();
        }

        emit cardPlayedSignal(playerIndex, cardIndex);
    }

    if (type == "turnEnded")
    {
        int currentPlayer = -1;
        if (json.contains("currentPlayer"))
        {
            currentPlayer = json["currentPlayer"].toInt();
        }
        emit setCurrentPlayer(currentPlayer);
    }

    if (type == "colorChanged")
    {
        QString color;
        if (json.contains("color"))
        {
            color = json["color"].toString();
        }
        emit setColorSignal(color);
    }

    if (type == "calledUno")
    {
        int playerIndex = -1;
        if (json.contains("playerIndex"))
        {
            playerIndex = json["playerIndex"].toInt();
        }
        emit unoCalledSignal(playerIndex);
    }

    if (type == "gameEnded")
    {
        int winner = -1;
        if (json.contains("winner"))
        {
            winner = json["winner"].toInt();
        }
        emit gameOver(winner);
    }

    if  (type == "msg")
    {
        int playerIndex = -1;
        if (json.contains("playerIndex"))
        {
            playerIndex = json["playerIndex"].toInt();
        }
        QString msg = json.value("text").toString();
        emit sendMsgSignal(playerIndex, msg);
    }

    qDebug() << QJsonDocument(json).toJson(QJsonDocument::Compact);
}

void ServerCommunicationManager::onDeckClicked(int playerIndex)
{
    QJsonObject jsonObject;
    jsonObject["type"] = "deckClicked";
    jsonObject["playerIndex"] = QString::number(playerIndex);
    sendRequest(jsonObject);
}

void ServerCommunicationManager::onCardClicked(int playerIndex, int handCardIndex)
{
    QJsonObject jsonObject;
    jsonObject["type"] = "cardClicked";
    jsonObject["playerIndex"] = QString::number(playerIndex);
    jsonObject["handCardIndex"] = QString::number(handCardIndex);

    sendRequest(jsonObject);
}

void ServerCommunicationManager::onDialogWildCardClicked(QString color)
{
    QJsonObject jsonObject;
    jsonObject["type"] = "dialogWildCard";
    jsonObject["color"] = color;
    sendRequest(jsonObject);
}

void ServerCommunicationManager::onUnoClicked(int playerIndex)
{
    QJsonObject jsonObject;
    jsonObject["type"] = "unoClicked";
    jsonObject["playerIndex"] = QString::number(playerIndex);
    sendRequest(jsonObject);
}

void ServerCommunicationManager::onMsgClicked(int playerIndex,QString txt)
{
    QJsonObject jsonObject;
    jsonObject["type"] = "msg";
    jsonObject["playerIndex"] =QString::number(playerIndex);
    jsonObject["text"] = txt;
    sendRequest(jsonObject);
}
