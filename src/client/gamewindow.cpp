#include "gamewindow.h"
#include "ui_gamewindow.h"
#include "mainwindow.hpp"
#include "playeritem.h"
#include "card.h"
#include "carditem.h"
#include "servercommunicationmanager.h"
#include "dialogcolor.h"
#include <iostream>
#include <string>
#include <QLabel>
#include <QComboBox>
#include <QBrush>
#include <QColor>
#include <QRegularExpression>
#include <QTimer>

GameWindow::GameWindow(QVector<QString> playerNamesList, int mainPlayerIndex, QWidget *parent, QString imgPath)
    : m_playerNamesList(playerNamesList)
    , m_mainPlayerIndex(mainPlayerIndex)
    , m_chatModel(new QStandardItemModel(this))
    , QMainWindow(parent)
    , m_imgPath(imgPath)
    , ui(new Ui::GameWindow)
{
    ui->setupUi(this);
    m_chatModel->insertColumn(0);
    ui->chatView->setModel(m_chatModel);

    SetUpAppearance(m_imgPath);

    ConnectSceneAndView(m_playerNamesList);

    //Music
    mediaPlayer = new QMediaPlayer(this);
    audioOutput = new QAudioOutput(this);
    mediaPlayer->setAudioOutput(audioOutput);
    mediaPlayer->setSource(QUrl::fromLocalFile("../../resources/music/UNO_music.mp3"));
    audioOutput->setVolume(50);
    mediaPlayer->play();

    // Signals and slots
    connect(ui->pbDeck, &QPushButton::clicked, this, [=](){
        qDebug() << m_mainPlayerIndex;
        ServerCommunicationManager::instance().onDeckClicked(m_mainPlayerIndex);});
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::cardDealtSignal, this, &GameWindow::addNewCard);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::initializePileSignal, this, &GameWindow::initializePile);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::cardPlayedSignal, this, &GameWindow::cardPlayed);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::setColorSignal, this, &GameWindow::setColor);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::setCurrentPlayer, this, &GameWindow::initTurn);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::unoCalledSignal, this, &GameWindow::unoCalled);
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::gameOver, this, [=](int winner){ui->lblGameOver->setText(m_playerNamesList[winner] + " won! woohoo!!!");
    ui->pb->show(); });
    connect(&ServerCommunicationManager::instance(), &ServerCommunicationManager::sendMsgSignal,this,&GameWindow::msgCalled);
    connect(ui->volumeSlider, &QSlider::valueChanged, this, &GameWindow::on_volumeSlider_valueChanged);


    if (mainPlayerIndex != 0){
        ui->pbDeck->setDisabled(true);
    }

    ui->pb->hide();

}

GameWindow::~GameWindow()
{
    delete ui;
    delete mediaPlayer;
    delete audioOutput;
}


void GameWindow::addNewCard(int index, QString cardType, QString color, int value)
{
    Card* card = nullptr;

    card = CreateCard(cardType, color, value);

    const auto item = new CardItem(card);

    if (index != m_mainPlayerIndex){
        item->setVisible(false);
    }

    m_players[index]->addItem(item);

    emit newCardAdded(item, index);
}

void GameWindow::CardPressed(CardItem *item){

    if (activePlayer != mainPlayerIndex()){
        return;
    }


    PlayerItem *player = qobject_cast<PlayerItem *>(m_players[m_mainPlayerIndex]);
    int handCardIdx = player->cardItems().indexOf(item);

    ServerCommunicationManager::instance().onCardClicked(m_mainPlayerIndex, handCardIdx);

    return;
}

void GameWindow::cardPlayed(int playerIndex, int cardIndex){

    ui->leActiveColor->clear();
    PlayerItem *player = qobject_cast<PlayerItem *>(m_players[playerIndex]);
    CardItem* cardItem = player->cardItems().at(cardIndex);

    if(playerIndex == mainPlayerIndex() && cardItem->card()->colorName().compare(QString::fromStdString("BLACK")) == 0){
        OpenDialogChooseColor();
    }

    ChangePileCard(cardItem);

    emit RemoveCard(cardItem);
}

void GameWindow::setColor(QString color){
    ui->leActiveColor->setText(color);
}

void GameWindow::OpenDialogChooseColor()
{
    DialogColor* dialog = new DialogColor(this);

    dialog->exec();
}


void GameWindow::ChangePileCard(CardItem *item)
{
    QString path = CardItem::LoadImage(item->card());

    QPixmap pixmapPile(path);
    QIcon pbPileIcon(pixmapPile);
    ui->pbPile->setIcon(pbPileIcon);
    ui->pbPile->setIconSize(ui->pbPile->size());
}

void GameWindow::initializePile(QString cardType, QString color, int value)
{
    Card* card = nullptr;
    card = CreateCard(cardType, color, value);
    const auto item = new CardItem(card);
    ChangePileCard(item);
    delete item;
}

void GameWindow::initTurn(int currPlayerIdx){

    activePlayer=currPlayerIdx;

    if (currPlayerIdx ==  mainPlayerIndex()){
        ui->pbDeck->setDisabled(false);
    }else{
        ui->pbDeck->setDisabled(true);
    }
}

int GameWindow::mainPlayerIndex() const
{
    return m_mainPlayerIndex;
}

void GameWindow::SetUpAppearance(QString imgPath)
{
    // Background
    QPixmap bkgnd(imgPath);
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);

    // Deck image
    QPixmap pixmapDeck("../../resources/images/cards/back.jpeg");
    QIcon pbDeckIcon(pixmapDeck);
    ui->pbDeck->setIcon(pbDeckIcon);
    ui->pbDeck->setIconSize(ui->pbDeck->size());
}

void GameWindow::ConnectSceneAndView(QVector<QString> playerNames)
{
    QList<QLabel *> playerLabels = ui->centralWidget->findChildren<QLabel *>(QRegularExpression("^lblPlayer"));
    int k = 0;
    int n = playerNames.size();

    // Setting player names

    for (int j = 0, i = 0; i < n; i++, j++){

        k = (i + m_mainPlayerIndex) % n;
        playerLabels[j]->setText(playerNames[k]);
        m_players.append(new PlayerItem(this));

        // Two players will be oposite each other
        if(n == 2){
            j++;
        }
    }

    // Connecting scene and view
    QList <QGraphicsView *> playerGraphicsView = ui->centralWidget->findChildren<QGraphicsView *>(QRegularExpression("^gvPlayer"));

    for (int j = 0, i = 0; i < n; i++, j++){

        k = (i+ m_mainPlayerIndex) % n;

        dynamic_cast<PlayerItem *>(m_players[k])->setPlayerIndex(k);

        playerGraphicsView[j]->setScene(m_players[k]);
        playerGraphicsView[j]->setRenderHint(QPainter::Antialiasing);

        connect(this, &GameWindow::newCardAdded, dynamic_cast<PlayerItem *>(m_players[k]), &PlayerItem::newCardAdded);
        connect(this, &GameWindow::RemoveCard, dynamic_cast<PlayerItem *>(m_players[k]), &PlayerItem::RemoveCard);

        connect(dynamic_cast<PlayerItem *>(m_players[k]), &PlayerItem::CardPressed, this, &GameWindow::CardPressed);


        // Two players will be oposite each other
        if(n == 2){
            j++;
        }
    }
}

Card *GameWindow::CreateCard(QString cardType, QString color, int value)
{

    // // NUMERIC
    // Card(COLOR color, int value);

    // // SKIP, REVERSE, DRAW_TWO
    // Card(COLOR color, CARD_TYPE cardType);

    // // WILD, WILD_DRAW_FOUR
    // Card(CARD_TYPE cardType);

    Card* card = nullptr;

    Card::CARD_TYPE cardTypeEnum = Card::cardTypeStrToEnum(cardType);
    Card::COLOR colorEnum = Card::colorStrToEnum(color);

    if (cardTypeEnum == Card::CARD_TYPE::NUMERIC){
        card = new Card(colorEnum, value);
    }else if (cardTypeEnum == Card::CARD_TYPE::SKIP || cardTypeEnum == Card::CARD_TYPE::REVERSE || cardTypeEnum == Card::CARD_TYPE::DRAW_TWO){
        card = new Card(colorEnum, cardTypeEnum);
    }else if (cardTypeEnum == Card::CARD_TYPE::WILD|| cardTypeEnum == Card::CARD_TYPE::WILD_DRAW_FOUR){
        card = new Card(cardTypeEnum);
    }

    return card;
}


void GameWindow::on_pushButton_clicked()
{
    ServerCommunicationManager::instance().onUnoClicked(m_mainPlayerIndex);
}

void GameWindow::on_sendButton_clicked()
{
    QString msg = ui->messageEdit->text();
    if (msg.isEmpty()){
        return;
    }
    ServerCommunicationManager::instance().onMsgClicked(mainPlayerIndex(), msg);
}


QString GameWindow::selectedColor() const
{
    return m_selectedColor;
}

void GameWindow::setSelectedColor(const QString &newSelectedColor)
{
    m_selectedColor = newSelectedColor;
}


void GameWindow::unoCalled(int playerIndex)
{
    ui->lePlayerUno->setText(m_playerNamesList[playerIndex] + " called Uno!");

    QTimer *timer = new QTimer(this);

    connect(timer, &QTimer::timeout, this, [=]() {
        ui->lePlayerUno->clear();
        timer->deleteLater();
    });

    timer->start(3000);
}

void GameWindow::msgCalled(int playerIndex,const QString &text){

    int newRow = m_chatModel->rowCount();
    QFont boldFont;
    boldFont.setBold(true);

    m_chatModel->insertRows(newRow, 2);
    // set the alignment for the username
    m_chatModel->setData(m_chatModel->index(newRow, 0), int(Qt::AlignLeft | Qt::AlignVCenter), Qt::TextAlignmentRole);
    // set the for the username
    m_chatModel->setData(m_chatModel->index(newRow, 0), boldFont, Qt::FontRole);
    ++newRow;
    m_chatModel->setData(m_chatModel->index(newRow, 0), m_playerNamesList[playerIndex]+": "+text);
    // set the alignment for the message
    m_chatModel->setData(m_chatModel->index(newRow, 0), int(Qt::AlignLeft | Qt::AlignVCenter), Qt::TextAlignmentRole);
    // scroll the view to display the new message
    ui->chatView->scrollToBottom();
    ui->messageEdit->clear();
}

void GameWindow::on_volumeSlider_valueChanged(int value)
{
    if (audioOutput)
    {
        qreal volume = static_cast<qreal>(value) / 100.0;
        audioOutput->setVolume(volume);
    }
}

void GameWindow::closeEvent(QCloseEvent *event)
{
    // Accept the close event
    event->accept();

    // Close the application
    qApp->quit();
}

void GameWindow::on_pb_clicked()
{
    // Go back to main menu
    MainWindow* mainwindow = dynamic_cast<MainWindow*>(parent());
    mainwindow->setCurrentPage(MainWindow::Page::Menu);
    mainwindow->show();
    this->hide();
    delete this;
}

