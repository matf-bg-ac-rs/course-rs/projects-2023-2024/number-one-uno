# Number one UNO

Players take turns matching cards by number or color while trying to be the first to empty their hand. Special action cards add twists to the game, like skip, reverse, and wild cards, making it a fun and strategic multiplayer experience for all ages.

| ![img](resources/game.png) |
| --- |


## Requirements:
- ![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B%2017-blue)
- ![CMake](https://img.shields.io/badge/Build-CMake%203%2E5-red)
- ![qt6](https://img.shields.io/badge/Framework-Qt6-green)
- ![Qt6::network](https://img.shields.io/badge/Library-Qt6::network-green)
- ![Qt6::network](https://img.shields.io/badge/Library-Qt6::multimedia-green)


## How to run the app :hammer:
- In the terminal, position yourself in the desired directory
- Clone the repository with the command: $ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/number-one-uno.git
- Open the Qt Creator environment and open the file on path `./number-one-uno/game_server/CMakeLists.txt`
- Press the Run button in the lower left corner of the screen
- In Qt creator select `File` `->` `Open File or Project` and select `./number-one-unosrc/app/CMakeLists.txt`
- For each client player press the Run button in the lower left corner of the screen (one player creates a game and the others join the game)

## Demo Video :cinema: [here](https://www.youtube.com/watch?v=FA15wxAbM0g)


## Developers :
 - <a href="https://gitlab.com/m97s">Matija Srećković 167/2016</a>
 - <a href="https://gitlab.com/veljko13strugar">Veljko Strugar 399/2021</a>
 - <a href="https://gitlab.com/aleksa.kostur">Aleksa Kostur 44/2018</a>
 - <a href="https://gitlab.com/milana-vukovic">Milana Vuković 1034/2023</a>
 - <a href="https://gitlab.com/cefika">Stefan Mitrović 350/2020</a>
 - <a href="https://gitlab.com/nikolamajstorovich">Nikola Majstorović 354/2019</a>
